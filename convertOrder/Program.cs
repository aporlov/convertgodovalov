﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Globalization;

namespace ConvertOrder
{
    class Program
    {
        static int Main(string[] args)
        {
//            string shablon = @"Клиент       : [CodeClient]
//Получатель   : [CodeDostavki]
//Оплата       : 1
//ID заказа    : 
//Клиентский ID: [ZakazCode]
//Дата заказа  : [DataOrder]
//Позиций      : [Rows]
//Версия EXE   : 
//Версия CFG   : 
//Статус CFG   : 
//Прайс-лист   : 
//Комментарий  : [Comment]
//";
           
            try
            {
                Encoding encoding = Encoding.GetEncoding(1251);
                #region Проверка параметров
                if (args.Length < 2)
                {
                    System.Console.WriteLine("Convertpanatec <файл заявки> <shablon>");
                    return 1;
                }

                if (!System.IO.File.Exists(args[0]))
                {
                    System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Не найден файл заявки");
                    return 1;
                }
                if (!System.IO.File.Exists(args[1]))
                {
                    System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Не найден файл шаблона");
                    return 1;
                }  
                #endregion
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Начинаем конвертацию. " + " " + args[0]);
                //Читаем файл заявки
                DataTable excel = ReadExcelToTable(args[0]);
                Order order = new Order();
                order.rows = new List<Row>();
                string Date = DateTime.Today.ToString("dd.MM.yyyy");
                string[] buffer = args[1].Split('/');
                int i=0;
                int counter=1;
                int num = int.Parse("A0", NumberStyles.AllowHexSpecifier);
                char cnum = (char)num;
                foreach (DataRow row in  excel.Rows)
                {
                    if (counter == 1)
                    {
                        order.ZakazCode = row[5].ToString();
                        order.DataOrder = DateTime.ParseExact(row[7].ToString(), "dd MMM yyyy", CultureInfo.CurrentCulture).ToString("dd.MM.yyyy");
                    }
                    if (counter >11)
                    {      

                        if (!string.IsNullOrEmpty(row[2].ToString()))
                            order.rows.Add(new Row() { Code = row[2].ToString().Replace(cnum.ToString(), String.Empty), Count = row[6].ToString().Replace(cnum.ToString(), String.Empty).Replace(" ", String.Empty) });
                    }

                    counter++;
                }           
                //формируем заявку СИА
                //формируем заявку СИА
                string[] shablon = File.ReadAllLines(args[1], encoding);
               
                    StringBuilder ordersiaPanatec = new StringBuilder();
                    ordersiaPanatec.Append(string.Join("\r\n", shablon).Replace("[CodeDostavki]", order.CodeDostavki)
                                                   .Replace("[CodeClient]", order.CodeClient)
                                                   .Replace("[DataOrder]", order.DataOrder)
                                                   .Replace("[Rows]", order.rows.Count.ToString())
                                                   .Replace("[CodeZakaz]", order.ZakazCode)
                                                   .Replace("[Comment]", order.Comment)).AppendLine();
                    foreach (var item in order.rows)
                    {
                        ordersiaPanatec.Append(new string(' ', 50 - item.Code.Length)).Append(item.Code).Append(new string(' ', 10 - item.Count.Length)).Append(item.Count).AppendLine();
                    }
                    //пишем в файл 
                    File.WriteAllText(Path.Combine(Path.GetDirectoryName(args[0]), Path.GetFileName(args[0])+ ".txt"), ordersiaPanatec.ToString(), encoding);                
                

            }
            catch (Exception e)
            {
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + e.Message);
                return 1;
            }



            return 0;
        }
        public class Order
        {
            public string CodeDostavkiPokupatel { get; set; }
            public string CodeClient { get; set; }
            public string ZakazCode { get; set; }
            public string CodeDostavki { get; set; }
            public string AddressDostavki { get; set; }
            public string Rows { get; set; }
            public string DataOrder { get; set; }
            public string TimeOrder { get; set; }
            public string DataPrice { get; set; }
            public string TimePrice { get; set; }
            public string Comment { get; set; }
            public int Index {get;set;}
            public List<Row> rows { get; set; }
        }
        public class Row
        {
            public string Code { get; set; }
            public string Count { get; set; }
            public string CodeDostavkiFarm { get; set; }
        }
       
              public static  DataTable ReadExcelToTable(string path)
        {

            //Connection String

            //string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";
            //the same name 
            //string connstring = Provider=Microsoft.JET.OLEDB.4.0;Data Source=" + path + //";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';"; 

            //using (OleDbConnection conn = new OleDbConnection(connstring))
            //{
                //conn.Open();
                ////Get All Sheets Name
                //DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });

                ////Get the First Sheet Name
                //string firstSheetName = sheetsName.Rows[0][2].ToString();

                ////Query String 
                //string sql = string.Format("SELECT * FROM [{0}]", firstSheetName);
                //OleDbDataAdapter ada = new OleDbDataAdapter(sql, connstring);
                //DataSet set = new DataSet();
                //ada.Fill(set);
                //return set.Tables[0];            
                    //System.Data.OleDb.OleDbConnection MyConnection;
        //}
            using (System.Data.OleDb.OleDbConnection connExcel = new System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + path + "';Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1;\""))
            {
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "TABLE" });
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                DataSet ds = new DataSet();
                System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter("select  * from [" + SheetName + "]", connExcel);
                MyCommand.Fill(ds);
                return ds.Tables[0];
            }
            
        }
            
      
        public class Cod
        {
            public string CodeDostavki { get; set; }
            public string CodeClient { get; set; }
        }
    }
}
